<?php
/**
 * @file
 * Text filter for converting media-links into iframes.
 */

/**
 * Implements hook_help().
 */
function mediafilter_help($path, $arg) {
  switch ($path) {
    case 'admin/help#mediafilter':

      $markup = '<p>' . t("Mediafilter is a module that extends existing textformats like Filtered HTML or Full HTML. It converts Youtube-links to embedded videos using the Youtube HTML5 api. It doesn't use the shockwave/flashplayer, IE7 and lower are not supported this way, but mobile devices are.") . '</p>';
      $markup .= '<p>' . t("Enable this filter in a text-format. The weight of this filter must be lower than 'Limit allowed HTML tags'. Users can post all kinds of Youtube-links and this filter will convert it to an embedded video in an iframe. Admins can set some per textformat options, like width, height, allowfullscreen for the iframe.") . '</p>';
      return $markup;

  }
}

/**
 * Processes Youtube links.
 */
function mediafilter_process_yt($text) {

  // Build the iframe.
  // @todo: insert all possible options options.
  // @url: developers.google.com/youtube/player_parameters?playerVersion=HTML5.
  $iframe = '<div class="mediafilter youtube"><iframe id="player" width="yt_width" height="yt_height" allowfullscreen="0" frameborder="0"';
  $iframe .= ' src="//www.youtube.com/embed/' . $text . '?enablejsapi=1';
  $iframe .= '&origin=' . url('/', array('absolute' => TRUE));
  $iframe .= '&fs=yt_fs';
  $iframe .= '&autohide=yt_autohide';
  $iframe .= '&autoplay=yt_autoplay';
  $iframe .= '&color=yt_color';
  $iframe .= '&controls=yt_controls';
  $iframe .= '&loop=yt_loop';
  $iframe .= '&modestbranding=yt_branding';
  $iframe .= '&rel=yt_rel';
  $iframe .= '&theme=yt_theme';
  $iframe .= '"></iframe></div>';
  return $iframe;
}


/**
 * Processes Youtube links.
 */
function mediafilter_process_vm($text) {

  // Build the iframe.
  // @todo: insert all possible options options.
  // @url: http://developer.vimeo.com/player/embedding.
  $iframe = '<div class="mediafilter vimeo"><iframe id="vimeo-player" width="vm_width" height="vm_height" webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder="0"';
  $iframe .= ' src="//player.vimeo.com/video/' . $text;
  $iframe .= '?autopause=vm_autopause';
  $iframe .= '&autoplay=vm_autoplay';
  $iframe .= '&badge=vm_badge';
  $iframe .= '&byline=vm_byline';
  $iframe .= '&color=vm_color';
  $iframe .= '&loop=vm_loop';
  $iframe .= '&player_id=' . url('/', array('absolute' => TRUE));
  $iframe .= '&portrait=vm_portrait';
  $iframe .= '&title=vm_title';
  $iframe .= '"></iframe></div>';
  return $iframe;
}


/**
 * Callback to replace content of youtube elements.
 */
function _mediafilter_process_yt_callback($matches) {
  return mediafilter_process_yt($matches[1]);
}

/**
 * Callback to replace content of vimeo elements.
 */
function _mediafilter_process_vm_callback($matches) {
  return mediafilter_process_vm($matches[1]);
}

/**
 * Escape Youtube links.
 */
function mediafilter_escape_yt($text) {
  // Add mediafilter escape tags.
  $text = "[mediafilter_yt]{$text}[/mediafilter_yt]";
  return $text;
}

/**
 * Escape Vimeo links.
 */
function mediafilter_escape_vm($text) {
  // Add mediafilter escape tags.
  $text = "[mediafilter_vm]{$text}[/mediafilter_vm]";
  return $text;
}

/**
 * Callback to escape content of Youtube elements.
 */
function _mediafilter_escape_yt_callback($matches) {
  return mediafilter_escape_yt($matches[1]);
}

/**
 * Callback to escape content of Vimeo elements.
 */
function _mediafilter_escape_vm_callback($matches) {
  return mediafilter_escape_vm($matches[1]);
}

/**
 * Implements hook_filter_info().
 */
function mediafilter_filter_info() {
  $filters['mediafilter'] = array(
    'title' => t('Media filter'),
    'description' => t('Allows users to post media-links which will be converted to inline media-elements.'),
    'prepare callback' => '_mediafilter_prepare',
    'process callback' => '_mediafilter_process',
    'settings callback' => '_mediafilter_settings',
    'tips callback' => '_mediafilter_tips',
    'default settings' => array(
      'yt_width' => '560px',
      'yt_height' => '350px',
      'yt_settings' => array(),
      'vm_width' => '560px',
      'vm_height' => '350px',
      'vm_color' => '00adef',
      'vm_settings' => array(),
    ),
    'cache' => TRUE,
  );
  return $filters;
}


/**
 * Implements hook_filter_FILTER_prepare().
 *
 * Escape media-elements.
 */
function _mediafilter_prepare($text, $format) {

  // Youtube links.
  $text = preg_replace_callback('#(?:https?:\/\/|www\.|m\.|^)?youtu(?:be\.com\/watch\?(?:.*?&(?:amp;)?)?v=|\.be\/)([\w‌​\-]+)(?:&(?:amp;)?[\w\?=]*)?#s', '_mediafilter_escape_yt_callback', $text);
  // Vimeo links.
  $text = preg_replace_callback('#(?:https?:\/\/(?:[\w]+\.)*vimeo\.com(?:[\/\w]*\/videos?)?\/([0-9]+)[^\s]*)#s', '_mediafilter_escape_vm_callback', $text);

  // Replace leading http or https (do not know why that is still here...).
  $text = preg_replace('#(https?:\/\/)?\[mediafilter#', '[mediafilter', $text);
  return $text;
}


/**
 * Implements hook_filter_FILTER_process().
 */
function _mediafilter_process($text, $filter, $format) {

  // Youtube-links.
  if (preg_match('#\[mediafilter_yt\](.*?)\[/mediafilter_yt]#s', $text)) {
    $text = preg_replace_callback('#\[mediafilter_yt\](.*?)\[/mediafilter_yt]#s', '_mediafilter_process_yt_callback', $text);

    // Get vars and insert in text.
    $settings = array(
      'yt_width' => check_plain($filter->settings['yt_width']),
      'yt_height' => check_plain($filter->settings['yt_height']),
      'yt_fs' => $filter->settings['yt_settings']['yt_fs'] ? 0 : 1,
      'yt_autohide' => $filter->settings['yt_settings']['yt_autohide'] ? 1 : 2,
      'yt_autoplay' => $filter->settings['yt_settings']['yt_autoplay'] ? 1 : 0,
      'yt_color' => $filter->settings['yt_settings']['yt_color'] ? 'white' : 'red',
      'yt_controls' => $filter->settings['yt_settings']['yt_controls'] ? 0 : 1,
      'yt_loop' => $filter->settings['yt_settings']['yt_loop'] ? 1 : 0,
      'yt_branding' => $filter->settings['yt_settings']['yt_branding'] ? 1 : 0,
      'yt_rel' => $filter->settings['yt_settings']['yt_rel'] ? 1 : 0,
      'yt_theme' => $filter->settings['yt_settings']['yt_theme'] ? 'light' : 'dark',
    );
    foreach ($settings as $name => $value) {
      $text = str_replace($name, $value, $text);
    }

  }

  // Vimeo links.
  if (preg_match('#\[mediafilter_vm\](.*?)\[/mediafilter_vm]#s', $text)) {
    $text = preg_replace_callback('#\[mediafilter_vm\](.*?)\[/mediafilter_vm]#s', '_mediafilter_process_vm_callback', $text);

    // Get vars and insert in text.
    $settings = array(
      'vm_width' => check_plain($filter->settings['vm_width']),
      'vm_height' => check_plain($filter->settings['vm_height']),
      'vm_color' => urlencode(check_plain($filter->settings['vm_color'])),
      'vm_autopause' => $filter->settings['vm_settings']['vm_autopause'] ? 1 : 0,
      'vm_autoplay' => $filter->settings['vm_settings']['vm_autoplay'] ? 1 : 0,
      'vm_badge' => $filter->settings['vm_settings']['vm_badge'] ? 1 : 0,
      'vm_byline' => $filter->settings['vm_settings']['vm_byline'] ? 1 : 0,
      'vm_loop' => $filter->settings['vm_settings']['vm_loop'] ? 1 : 0,
      'vm_portrait' => $filter->settings['vm_settings']['vm_portrait'] ? 1 : 0,
      'vm_title' => $filter->settings['vm_settings']['vm_title'] ? 1 : 0,
    );
    foreach ($settings as $name => $value) {
      $text = str_replace($name, $value, $text);
    }
  }
  return $text;
}

/**
 * Implements hook_filter_FILTER_tips().
 */
function _mediafilter_tips($format, $long = FALSE) {
  return t('Youtube and Vimeo links will be displayed as inline videos');
}

/**
 * Settings callback for the mediafilter filter.
 *
 * @see hook_filter_info()
 * @see hook_filter_FILTER_settings()
 */
function _mediafilter_settings($form, &$form_state, $filter, $format, $defaults) {

  $filter->settings += $defaults;
  $elements = array();
  $elements['youtube'] = array(
    '#markup' => '<h4>' . t('Youtube settings') . '</h4>',
  );
  $elements['yt_width'] = array(
    '#type' => 'textfield',
    '#maxlength' => 6,
    '#size' => 6,
    '#title' => t('Youtube iframe width'),
    '#description' => t('By default Youtube inline video is an iframe of 560pixels wide'),
    '#default_value' => $filter->settings['yt_width'],
  );
  $elements['yt_height'] = array(
    '#type' => 'textfield',
    '#maxlength' => 6,
    '#size' => 6,
    '#title' => t('Youtube iframe height'),
    '#description' => t('By default Youtube inline video is an iframe of 315 pixels high'),
    '#default_value' => $filter->settings['yt_height'],
  );
  $elements['yt_settings'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Youtube toggle settings'),
    '#options' => array(
      'yt_fs' => t('Disable fullscreen player'),
      'yt_autohide' => t('Autohide player controls when playing'),
      'yt_autoplay' => t('Autoplay as soon as video is loaded'),
      'yt_color' => t('Set the color of the progressbar to white'),
      'yt_controls' => t('Hide the player controls'),
      'yt_loop' => t('Loop the video (HTML5 player only)'),
      'yt_branding' => t('Hide the Youtube logo'),
      'yt_rel' => t('Display related videos at end of video'),
      'yt_theme' => t('Display a light controlbar instead of dark'),
    ),
    '#default_value' => $filter->settings['yt_settings'],
  );

  $elements['vimeo'] = array(
    '#markup' => '<h4>' . t('Vimeo settings') . '</h4>',
    '#description' => t('If the owner of a video is a Plus member, some of these settings may be overridden by their preferences.'),
  );
  $elements['vm_width'] = array(
    '#type' => 'textfield',
    '#maxlength' => 6,
    '#size' => 6,
    '#title' => t('Vimeo iframe width'),
    '#description' => t('By default Vimeo inline video is an iframe of 560pixels wide'),
    '#default_value' => $filter->settings['vm_width'],
  );
  $elements['vm_height'] = array(
    '#type' => 'textfield',
    '#maxlength' => 6,
    '#size' => 6,
    '#title' => t('Vimeo iframe height'),
    '#description' => t('By default Vimeo inline video is an iframe of 315 pixels high'),
    '#default_value' => $filter->settings['vm_height'],
  );
  $elements['vm_color'] = array(
    '#type' => 'textfield',
    '#maxlength' => 6,
    '#size' => 6,
    '#title' => t('Specify the color of the video controls. Default is 00adef'),
    '#description' => t('Use HEX notation, do not include the #'),
    '#default_value' => $filter->settings['vm_color'],
  );
  $elements['vm_settings'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Vimeo toggle settings'),
    '#options' => array(
      'vm_autopause' => t('Enable pausing video when another video is played'),
      'vm_autoplay' => t('Autoplay as soon as video is loaded. (Note that this won’t work on some devices.)'),
      'vm_badge' => t('Enable the badge on the video.'),
      'vm_byline' => t('Show the user’s byline on the video'),
      'vm_loop' => t('Loop the video.'),
      'vm_portrait' => t('Show the user’s portrait on the video.'),
      'vm_title' => t('Show the title on the video'),
    ),
    '#default_value' => $filter->settings['vm_settings'],
  );
  $elements['vimeo_footnote'] = array(
    '#markup' => '<em>' . t('Note: If the owner of a video is a Vimeo-Plus member, some of these settings may be overridden by their preferences.') . '</em>',
  );

  return $elements;
}
