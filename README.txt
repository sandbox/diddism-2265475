CONTENTS OF THIS FILE
---------------------
* Introduction
Mediafilter is textformat-filter that converts medialinks to embedded content.
Right now only embedding youtube is supported.

* Requirements
There are no extra requirements for this plugin, it works standalone.

* Installation
- Download and extract the Mediafilter-module as usual.
- Enable the module @ /admin/modules.
- Go to admin/config/content/formats
- Enable this filter at a text-format e.g. for Filtered HTML.
- Medialinks written in that text-format will be converted to embedded content.

* Configuration
This is pretty self-explanatory I hope. Post an issue if not.

* Todo:
 - Get filtersettings inside textformatting function.
 - Add other providers like Vimeo.
